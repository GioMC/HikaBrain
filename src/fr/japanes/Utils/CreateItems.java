package fr.japanes.Utils;

import fr.japanes.HikaBrain;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.LeatherArmorMeta;

/**
 * Created by Gio on 26/12/2017.
 */
public class CreateItems {

    GetValues getValues = new GetValues();

    public CreateItems(Material material, Player player, int i, int count, Color color){

        System.out.println(getValues.getxRed());
        ItemStack itemStack;
        if(count == 1){
            if(color == null){
                /**
                 * Set a simple item if i == 1 and if color == null
                 */
                itemStack = new ItemStack(material);
            }else{
                /**
                 * Set color if the Color not == null
                 */
                itemStack = new ItemStack(material);
                LeatherArmorMeta itemMeta = (LeatherArmorMeta)itemStack.getItemMeta();
                itemMeta.setColor(color);
                itemStack.setItemMeta(itemMeta);
            }

        }else{
            /**
             * Set item with count if color == null
             */
           itemStack = new ItemStack(material, count);
        }


        /**
         * Infos
         *
         *  i = 1 = helmet
         *  i = 2 = chesplate
         *  i = 3 = leggings
         *  i = 4 = boots
         */

        if(i != 0){
            if(i == 1){
                player.getInventory().setHelmet(itemStack);
            }

            if(i == 2){
                player.getInventory().setChestplate(itemStack);
            }

            if(i == 3){
                player.getInventory().setLeggings(itemStack);
            }

            if(i == 4){
                player.getInventory().setBoots(itemStack);
            }
        }
        else
        {
            player.getInventory().addItem(itemStack);
        }
    }


}
