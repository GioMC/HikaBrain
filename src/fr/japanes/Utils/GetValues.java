package fr.japanes.Utils;

import fr.japanes.HikaBrain;
import org.bukkit.configuration.Configuration;

/**
 * Created by Gio on 28/12/2017.
 */
public class GetValues {

    public  int xRed ;
    public  int yRed = 0;
    public  int zRed = 0;

    public  int xBlue = 0;
    public  int yBlue = 0;
    public  int zBlue = 0;

    Configuration fileConfiguration = HikaBrain.hikaBrain.getConfig();

    /**
     * Inizialise class
     */


    public GetValues(){
        this.xRed = Integer.parseInt(fileConfiguration.getString("red.x"));
        this.yRed = Integer.parseInt(fileConfiguration.getString("red.y"));
        this.zRed = Integer.parseInt(fileConfiguration.getString("red.z"));

        this.xBlue = Integer.parseInt(fileConfiguration.getString("blue.x"));
        this.yBlue = Integer.parseInt(fileConfiguration.getString("blue.y"));
        this.zBlue = Integer.parseInt(fileConfiguration.getString("blue.z"));
    }


    public int getxRed(){
        return xRed;
    }

    public int getyRed(){
        return yRed;
    }

    public int getzRed(){
        return zRed;
    }


    public int getxBlue(){
        return xBlue;
    }

    public int getyBlue(){
        return yBlue;
    }

    public int getzBlue(){
        return zBlue;
    }



}
