package fr.japanes.Utils;

import org.bukkit.entity.Player;

import java.util.ArrayList;

/**
 * Created by Gio on 26/12/2017.
 */
public class Teams {
    public static ArrayList<Player> redTeam = new ArrayList<>();
    public static ArrayList<Player> blueTeam = new ArrayList<>();

    public static int getSizeRed(){
        return redTeam.size();
    }

    public static int getSizeBlue(){
        return blueTeam.size();
    }

    public static void addPlayerRed(Player player){
        redTeam.add(player);
    }

    public static void addPlayerBlue(Player player){
        blueTeam.add(player);
    }

    public static void RemovePlayerRed(Player player){
        redTeam.remove(player);
    }

    public static void RemovePlayerBlue(Player player){
        blueTeam.remove(player);
    }

    public static boolean checkPlayerRed(Player player){
        if(redTeam.contains(player))
            return true;
        else
            return false;
    }

    public static boolean checkPlayerBlue(Player player){
        if(blueTeam.contains(player))
            return true;
        else
            return false;
    }

}
