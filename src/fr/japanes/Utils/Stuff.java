package fr.japanes.Utils;

import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.entity.Player;


/**
 * Created by Gio on 26/12/2017.
 */
public class Stuff {


    public Stuff(Player player, Color color){
        player.getInventory().clear();
        /**
         * Set stuff to players
         */
        CreateItems sword = new CreateItems(Material.IRON_SWORD, player, 0, 1, null);
        CreateItems gapple = new CreateItems(Material.GOLDEN_APPLE, player, 0, 64, null);
        CreateItems sandstone1 = new CreateItems(Material.SANDSTONE, player, 0, 64, null);
        CreateItems sandstone2 = new CreateItems(Material.SANDSTONE, player, 0, 64, null);
        CreateItems sandstone3 = new CreateItems(Material.SANDSTONE, player, 0, 64, null);
        CreateItems sandstone4 = new CreateItems(Material.SANDSTONE, player, 0, 64, null);

        CreateItems helmet = new CreateItems(Material.LEATHER_HELMET, player, 1, 1, color);
        CreateItems chestplate = new CreateItems(Material.LEATHER_CHESTPLATE, player, 2, 1, color);
        CreateItems leggins = new CreateItems(Material.LEATHER_LEGGINGS, player, 3, 1, color);
        CreateItems boots = new CreateItems(Material.LEATHER_BOOTS, player, 4, 1, color);
    }

}
