package fr.japanes.Events;

import fr.japanes.Utils.Teams;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * Created by Gio on 26/12/2017.
 */
public class PlayerLeave implements Listener {

    @EventHandler
    public void onPlayerLeave(PlayerQuitEvent event){
        Player player = event.getPlayer();
        if(Teams.checkPlayerBlue(player)){
            Teams.RemovePlayerBlue(player);
        }
        if(Teams.checkPlayerRed(player)){
            Teams.RemovePlayerRed(player);
        }
    }

}
